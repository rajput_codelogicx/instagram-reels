import './App.css';
import Video from './components/Video'
import { db } from './firebase'
import {useState,useEffect} from 'react'
import {
  collection,
  getDocs
} from 'firebase/firestore'

function App() {
 const [videos,setVideos] = useState([])

 useEffect(() => {
    const fetchPosts = async () => {
      try {
        // Get reference
        const listingsRef = collection(db, 'posts')
        // Execute query
        const querySnap = await getDocs(listingsRef)
        setVideos(querySnap.docs.map((doc) => doc.data()))
      } catch (error) {
        console.log(error)
      }
    }
    fetchPosts()
 }, [])

  return (
    <div className="app">
      <div className="app__videos">   
      {videos.map(
          ({ url, channel, description, song, likes, messages, shares,avatar }) => (
            <Video
              url={url}
              channel={channel}
              song={song}
              likes={likes}
              messages={messages}
              description={description}
              shares={shares}
              avatar={avatar}
            />
          )
        )}
      </div>
    </div>
  );
}

export default App;
