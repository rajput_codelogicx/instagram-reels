// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD1qYPz3RrqC1joPbBLBmV88xTmRcPWowQ",
  authDomain: "pik-pok-652df.firebaseapp.com",
  projectId: "pik-pok-652df",
  storageBucket: "pik-pok-652df.appspot.com",
  messagingSenderId: "982311994516",
  appId: "1:982311994516:web:3dc3b0c04018edf3f6e081"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore();
