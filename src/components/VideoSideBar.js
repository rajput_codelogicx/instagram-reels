
import "../css/VideoSideBar.css"
import {useState} from 'react'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import FavoriteIcon from '@mui/icons-material/Favorite';
import MessageIcon from '@mui/icons-material/Message';
import ShareIcon from '@mui/icons-material/Share';

function VideoSideBar({likes,comment,share}) {
    const [liked,setLiked] = useState(false)
    return (
        <div className="videoSidebar">
            <div className="videoSidebar__button">
            {liked ? (
                <FavoriteIcon fontSize="large" onClick={(e) => setLiked(false)}/>
             ) : <FavoriteBorderIcon fontSize="large" onClick={(e) => setLiked(true)}/>}
           <p>{liked ? likes + 1 : likes}</p>
            </div>
            <div className="videoSidebar__button">
            <MessageIcon fontSize="large" />
              <p>{comment}</p>
            </div>
            <div className="videoSidebar__button">
            <ShareIcon fontSize="large" />
             <p>{share}</p>
            </div>
        </div>
    )
}

export default VideoSideBar
