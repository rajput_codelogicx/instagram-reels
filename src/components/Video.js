import '../css/video.css'
import VideoFooter from '../components/VideoFooter'
import VideoSideBar   from '../components/VideoSideBar'
import VideoHeader from '../components/VideoHeader'
import {useRef,useState} from 'react'

function Video({ url, channel, description, song, likes, messages, shares,avatar }) {
  const [play,setPlay] = useState(false)  
  const videoRef = useRef(null)
  const onVideoPress = () => {
     if(play){
         videoRef.current.pause()
         setPlay(false)
     }else{
         videoRef.current.play()
         setPlay(true)
     }
  }

    return (
        <div className="video">
         <VideoHeader />    
            <video 
             className="video__player"
             loop
             ref={videoRef}
             onClick={onVideoPress}
             src={url}>
             </video>
           <VideoFooter channel={channel} description={description} song={song} avatar={avatar} />
           <VideoSideBar likes={likes} messages={messages} shares={shares} />
        
        </div>
    )
}

export default Video
