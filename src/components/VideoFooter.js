import '../css/VideoFooter.css';
import MusicNoteIcon from '@mui/icons-material/MusicNote';
import Ticker from 'react-ticker';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';

function VideoFooter({ channel, description, song, avatar }) {
	return (
		<div className="videoFooter">
			<div className="videoFooter__text">
				<div className="videoFooter_avtar">
					<Avatar src={avatar} />
					<h3>
						{channel}
						<Button variant="outlined" size="small">
							Follow
						</Button>
					</h3>
				</div>
				<p>{description}</p>
				<div className="videoFooter__ticker">
					<MusicNoteIcon className="videoFooter__icon" />
					<Ticker mode="smooth">{({ index }) => <p>{song}</p>}</Ticker>
				</div>
			</div>
			<img className="videoFooter__record" src="https://static.thenounproject.com/png/934821-200.png" alt="" />
		</div>
	);
}

export default VideoFooter;
